FROM 		tomcat:8.0.21-jre8

MAINTAINER 	Antonio Fco Herrera (tony_fht@hotmail.com)

COPY 		./target/openexpoeurope-1.0.0.war /usr/local/tomcat/webapps/
